#!/usr/bin/env python3
import os
import sys

cmd_template = "{} -m 100 '^.*\t.*\t{}\t' all_words_ngrams"  # | sort -t'\t' -r -k 1 -n"


def usage():
    print("./raw_query.py - execute raw regexes against the corpus")
    print("\tusage: ./raw_query.py '{rg|grep}( -P)?' '<query>'")
    print()
    print("\texample: ./raw_query.py 'rg -P' 'the new york times'")
    print()
    sys.exit(0)


if len(sys.argv) < 3:
    usage()

grep_cmd = sys.argv[1]
query = sys.argv[2]

# get rid of ripgrep's line numbers
if grep_cmd.startswith("rg"):
    grep_cmd += " -N"

f = os.popen(cmd_template.format(grep_cmd, query))

for line in f:
    sys.stdout.write(line)
