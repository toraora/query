import os

cmd_template = "rg -P -N --no-filename -m 500 '{query}' {corpus}"


def perform_query(query):
    print("Query: ", query)
    cmd = cmd_template.format(query=query, corpus="indexer_rs/output_final/")
    try:
        f = os.popen(cmd)
        for l in f:
            yield l
    except Exception as err:
        print("Error: ", err)
        return []
