import os
import time
import math
import functools

from flask import Flask, render_template, request

import engine_local
import engine_lambda
from query import parse_query, get_full_query
from rate_limiter import get_rate_limiter_for

app = Flask(__name__)
env = os.environ.get("FLASK_ENV")

engine = engine_lambda if env == "production" else engine_local
# perform_query = functools.lru_cache(maxsize=2048)(engine.perform_query)
perform_query = engine.perform_query


@app.errorhandler(404)
def page_not_found(error):
    return "There's nothing here!", 404


@app.route("/")
def home():
    return render_template("home.html", params={})


@app.route("/help")
def help():
    return render_template("help.html", params={})


@app.route("/query")
def query():
    params = get_params()
    if not params["query"]:
        return home()

    ip = request.remote_addr
    rate_limiter = get_rate_limiter_for(ip)
    if not rate_limiter.is_allowed():
        return render_template("429.html"), 429

    params["parsed_query"] = parse_query(params["query"].replace(" ", ""))

    start = time.time()
    results = format_results(perform_query(get_full_query(params)))
    end = time.time()
    elapsed = end - start
    elapsed = f"{elapsed:.4f}"

    return render_template("home.html", params=params, elapsed=elapsed, results=results)


def get_params():
    params = dict()
    for key in ["query", "pattern", "corpus", "engine", "partial"]:
        params[key] = request.args.get(key)
    return params


def format_results(results):
    output = []
    for l in results:
        l = l.strip()
        if not l:
            continue
        parts = l.split("\t")
        parts[0] = int(parts[0])
        parts.append(int(60 + 10 * (4 * math.log(parts[0]))) / 10)
        output.append(parts)
    return sorted(output, reverse=True)
