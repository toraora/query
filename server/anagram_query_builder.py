#!/usr/bin/env python3
from collections import defaultdict

import sys
import os

"""
anagrams in regex!

these are implemented using both positive and negative lookahead; for each part:
- add the positive lookahead (?=(.*(part)){7}) (where 7 is the frequency)
  - this ensures that the part shows up at least 7 times
- add the negative lookahead (?!.*(part)(.*\1){7}) (where 7 is the frequency)
  - this ensures that the part shows up at most 7 times
- add the part to the final matching query (part1|part2|part3){3}

This has some _huge_ limitations due to the way it works. Cases that should work perfectly:
- single character parts only (wildcards are fine). examples:
    - '<nationalbasketballleague>'
    - '<nationalbasketballAAAAAA>'
- any prefix together with a query of the previous type. examples:
    - '\w+<basketballleague>'
    - '\w+<basketballAAAAAA>'

Cases that may be buggy:
- any anagram together with a suffix. examples:
    - '<national>basketballleague'
- any anagram involving multi-character groups. examples:
    - '<(aa)(aaa)>' will match 'aaaa' since it:
        1) contains 'aa'
        2) contains 'aaa'
        3) consists of two instances of (aa|aaa)


negative lookahead: a, s, d, and f (independently) each must not show up 4 times
(?!.*([asdf])(.*\1){3})
(?!.*(?P<n3>[asdf])(.*(?P=n3)){3}) # named version

positive lookahead: a must show up at least 3 times
(?=(.*a){3})
"""


def parse_anagram_query(s):
    wild_count = s.count(".") + s.count("A") + s.count("\w")
    s = s.replace(".", "\w").replace("A", "\w")
    """
    only two allowed subtypes:
    1) chars
    2) groups
    """
    parts = []
    group_begin_idx = 0
    level = 0

    i = 0
    while i < len(s):
        c = s[i]
        if c == "(":
            if level == 0:
                group_begin_idx = i
            level += 1
        elif c == ")":
            level -= 1
            if level == 0:
                group = s[group_begin_idx : i + 1]
                parts.append(group)
        elif level == 0:
            if c == "\\":
                i += 1
                parts.append("\\{}".format(s[i]))
            elif c == "[":
                start = i
                while s[i] != "]":
                    i += 1
                group = s[start : i + 1]
                parts.append(group)
                wild_count += 1
            else:
                parts.append(c)

        i += 1

    return parts, wild_count


def join_options(options):
    if all([len(part) == 1 or part == "\\w" for part in options]):
        return "[" + "".join(options) + "]"
    return "({})".format("|".join(options))


def build(query):
    query, wilds = parse_anagram_query(query)

    chars = defaultdict(int)
    for c in query:
        chars[c] += 1

    s = ""

    # positive lookaheads enforce minimums for each query part
    all_parts = []
    chars_inverted = defaultdict(set)
    for part, freq in sorted(chars.items(), reverse=True):
        if len(part) == 1:
            chars_inverted[freq].add(part)
        all_parts.append(part)
        s += "(?=(.*{part}){{{freq}}})".format(part=part, freq=freq)

    # negative lookaheads enforce maximums for each query part
    # we can only use these for single character constraints
    # since, e.g. "aaaa" matches "aaa" twice
    for freq, c_set in chars_inverted.items():
        freq += wilds
        s += "(?!.*(?P<n{freq}>{options})(.*(?P=n{freq})){{{freq}}})".format(
            freq=freq, options=join_options(c_set)
        )

    s += "{}{{{}}}".format(join_options(all_parts), len(query))
    s = "(" + s + ")"

    return s


if __name__ == "__main__":
    s = build("sanfrancisco")

    print("regex: " + s)

    print()
    sys.exit(0)

    try:
        f = os.popen("./raw_query.py 'grep -P' '{}'".format(s))
        for l in f:
            sys.stdout.write(l)
    except:
        pass

    print()
