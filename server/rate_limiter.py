import functools
import time


class TicketingRateLimiter:
    def __init__(self, qps, max_tickets=None):
        if not max_tickets:
            max_tickets = max(1, 2 * qps)
        self.max_tickets = max_tickets
        self.num_tickets = max_tickets
        self.last_call = time.time()
        self.qps = qps

    def is_allowed(self):
        cur_time = time.time()
        new_tickets = (cur_time - self.last_call) * self.qps
        self.last_call = cur_time
        self.num_tickets = min(self.max_tickets, self.num_tickets + new_tickets)

        if self.num_tickets >= 1:
            self.num_tickets -= 1
            return True
        return False


@functools.lru_cache(maxsize=4096)
def get_rate_limiter_for(key):
    return TicketingRateLimiter(0.5, 2)
