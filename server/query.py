import anagram_query_builder

query_template = "^\d+\\t{pattern}\\t{query}$"


def get_full_query(params):
    query = params["parsed_query"]
    if params["partial"]:
        query = ".*" + query + ".*"
    pattern = params["pattern"] or ".*"
    full_query = query_template.format(query=query, pattern=pattern)
    print(full_query)
    return full_query


def parse_query(query):
    i = 0
    s = ""
    while i < len(query):
        c = query[i]
        if c == "<":
            i += 1
            anagram_sub_query = ""
            cc = query[i]
            while cc != ">":
                cc = query[i]
                if cc == "<":
                    raise Exception("Cannot nest anagram queries")
                anagram_sub_query += cc
                i += 1
            s += anagram_query_builder.build(anagram_sub_query[:-1]) + (
                query[i] if i < len(query) else ""
            )
        else:
            if c == "A":
                c = "."
            s += c
        i += 1

    return s
