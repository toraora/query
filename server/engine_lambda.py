import aiobotocore
import json
import asyncio

parts = [chr(97 + i // 26) + chr(97 + i % 26) for i in range(32)]


def perform_query(query):
    asyncio.set_event_loop(asyncio.new_event_loop())
    loop = asyncio.get_event_loop()
    return loop.run_until_complete(start_all_queries(query, loop))


async def start_all_queries(query, loop):
    tasks = [perform_single_query(query, part, loop) for part in parts]
    results = await asyncio.gather(*tasks)
    return [item for result in results for item in result]


async def perform_single_query(query, part_id, loop):
    session = aiobotocore.get_session(loop=loop)
    async with session.create_client("lambda", region_name="us-west-1") as client:
        payload = json.dumps({"query": query})
        response = await client.invoke(
            FunctionName=f"query-wiki-{part_id}",
            Payload=bytes(payload, encoding="utf-8"),
        )
        body = await response["Payload"].read()
        return body.decode("utf-8").split("\n")

