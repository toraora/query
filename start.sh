#!/usr/bin/env bash

export AWS_PROFILE=query
export FLASK_APP=server/server.py
export FLASK_ENV=development
flask run
