# Query

With flask installed (`yay -S python-flask` on Arch) and the requisite data files in `./wiki_query/`, simply run `./start.sh` and navigate to http://localhost:5000/.
