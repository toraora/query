#!/usr/bin/env bash

cd src/

PART_ID=$1

FUNCTION_ZIP=function-$PART_ID.zip
zip -j -u $FUNCTION_ZIP function.sh ../../wiki_query/part.$PART_ID.sorted

export AWS_PROFILE=tora

function create_layer() {
    zip -u -r base_layer.zip bootstrap jq rg
    aws lambda publish-layer-version \
        --layer-name query-base \
        --zip-file fileb://base_layer.zip
}

function create() {
    aws lambda create-function \
        --function-name query-wiki-$PART_ID \
        --zip-file fileb://$FUNCTION_ZIP \
        --handler function.handler \
        --runtime provided \
        --role arn:aws:iam::770010855585:role/lambda-query-read \
        --region us-west-1
}

function set_layer_config() {
    aws lambda update-function-configuration \
        --function-name query-wiki-$PART_ID \
        --layers arn:aws:lambda:us-west-1:770010855585:layer:query-base:4 \
        --timeout 10 \
        --memory-size 1536
}

function update() {
    aws lambda update-function-code \
        --function-name query-wiki-$PART_ID \
        --zip-file fileb://$FUNCTION_ZIP
}

function test() {
    aws lambda invoke --function-name query-wiki-$PART_ID --payload '{"query":".*(....).*\\1.*\\1.*"}' ../result-$PART_ID.log
}

# create_layer
# create
set_layer_config
# update
# test
