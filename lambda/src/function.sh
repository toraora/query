function handler() {
    RG_BIN=$RUNTIME_BASE_DIR/rg
    JQ_BIN=$RUNTIME_BASE_DIR/jq
    PART_ID=$(echo $AWS_LAMBDA_FUNCTION_NAME | cut -d- -f3)

    QUERY=$(echo $1 | $JQ_BIN -r '.query')

    $RG_BIN -m 500 -P -N --no-filename "$QUERY" $LAMBDA_TASK_ROOT/part.$PART_ID.sorted || true
}
