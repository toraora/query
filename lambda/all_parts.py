#!/usr/bin/env python3

for i in range(32):
    print(chr(97 + i // 26) + chr(97 + i % 26))
