import tables

type
    NodeObj = object
        children: Table[string, Node]
        count: int

    Node = ref NodeObj

    StringTrie* = object
        root: Node

proc newNode(): Node =
    let node = Node()
    node.children = initTable[string, Node](1)
    return node

proc getNext(n: Node, key: string): Node =
    if not n.children.hasKey(key):
        n.children[key] = newNode()
    return n.children[key]

proc incAll*(st: var StringTrie, words: seq[string]) =
    if st.root == nil:
        st.root = newNode()
    var cur = st.root
    for word in words:
        cur = cur.getNext(word)
        inc cur.count

proc get*(st: var StringTrie, words: openArray[string]): int =
    if st.root == nil:
        st.root = newNode()
    var cur = st.root
    for word in words:
        cur = cur.getNext(word)
    return cur.count

when isMainModule:
    import strutils

    var a = [
        "the fox",
        "the fox and the moon",
    ]

    var st = StringTrie()

    for s in a:
        var words = s.split(" ")
        st.incAll(words)

    assert st.get(["the", "fox"]) == 2
    assert st.get(["the", "fox", "and"]) == 1
    assert st.get(["the", "fox", "and", "the", "moon"]) == 1
