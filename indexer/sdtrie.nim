import tables

type
    NodeObj = object
        children: Table[int, Node]
        initialized: bool
        count: int

    Node = ref NodeObj

    StringDictTrie* = object
        root: Node
        words: Table[string, int]
        wordCount: int

var nodes*: int

proc newNode(): Node =
    inc nodes
    let node = Node()
    return node

proc getNext(n: Node, st: var StringDictTrie, key: string): Node =
    var intKey: int
    if key in st.words:
        intKey = st.words[key]
    else:
        st.words[key] = st.wordCount
        intKey = st.wordCount
        inc st.wordCount

    if not n.initialized:
        n.children = initTable[int, Node](1)
        n.initialized = true
    if not n.children.hasKey(intKey):
        n.children[intKey] = newNode()
    return n.children[intKey]

proc incAll*(st: var StringDictTrie, words: openArray[string]) =
    if st.root == nil:
        st.root = newNode()
    var cur = st.root
    for word in words:
        cur = cur.getNext(st, word)
        inc cur.count

proc get*(st: var StringDictTrie, words: openArray[string]): int =
    if st.root == nil:
        st.root = newNode()
    var cur = st.root
    for word in words:
        cur = cur.getNext(st, word)
    return cur.count

when isMainModule:
    import strutils

    var a = [
        "the fox",
        "the fox and the moon",
    ]

    var st = StringDictTrie()

    for s in a:
        var words = s.split(" ")
        st.incAll(words)

    assert st.get(["the", "fox"]) == 2
    assert st.get(["the", "fox", "and"]) == 1
    assert st.get(["the", "fox", "and", "the", "moon"]) == 1
