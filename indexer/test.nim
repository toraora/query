import ctrie
import strie
import sdtrie
import strutils
import timer
import re

const N = 5

var st = StringDictTrie()

var t = Timer()
t.start()

for i in 10 .. 99:
    let fname = "/home/nathanw/Projects/wikiextractor/text/AA/wiki_" & $i
    echo fname
    for l in lines(fname):
        let words = l.toLower().replace(re"[^A-Za-z ]").split(" ")
        for i in 0 ..< words.len:
            var j = min(words.len - 1, i + N)
            st.incAll(words[i ..< j])

echo st.get(["non"])
echo "elapsed: ", t.elapsed
echo "nodes created: ", sdtrie.nodes

echo readLine(stdin)

#[  lorem
    StringTrie:     30344 kb    (0.2322 sec)
    StringDictTrie: 24268 kb    (0.1642 sec)
    CharTrie:       131860 kb   (0.2889 sec)
]#

#[  wiki_10 .. wiki_19
    StringTrie:     579720 kb   (6.0950 sec)
    StringDictTrie: 457300 kb   (4.5246 sec)
    CharTrie:       3749720 kb  (8.0479 sec)
]#

#[  wiki_10 .. wiki_49
    StringDictTrie: 1325552 kb  (17.579 sec)
]#

#[  wiki_10 .. wiki_99
    StringDictTrie: 2523448 kb  (43.005 sec)
    StringDictTrie: 3971992 kb  (55.462 sec)    N = 5
]#