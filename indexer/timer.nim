import times

type Timer* = object
    start: float64

proc start*(t: var Timer) =
    t.start = cpuTime()

proc elapsed*(t: Timer): float64 =
    return cpuTime() - t.start