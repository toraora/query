type
    NodeObj = object
        children: array[0..25, Node]
        count: int

    Node = ref NodeObj

    CharTrie* = object
        root: Node

proc newNode(): Node =
    let n = Node()
    return n

proc getNext(n: Node, c: char): Node =
    let idx = ord(c) - 97
    if n.children[idx] == nil:
        n.children[idx] = newNode()
    return n.children[idx]

proc incAll*(ct: var CharTrie, words: openArray[string]) =
    if ct.root == nil:
        ct.root = newNode()
    var cur = ct.root
    for w in words:
        for c in w:
            cur = cur.getNext(c)
        inc cur.count

proc get*(ct: var CharTrie, words: openArray[string]): int =
    if ct.root == nil:
        ct.root = newNode()
    var cur = ct.root
    for w in words:
        for c in w:
            cur = cur.getNext(c)
    return cur.count


when isMainModule:
    import strutils

    var a = [
        "the fox",
        "the fox and the moon",
    ]

    var ct = CharTrie()

    for s in a:
        var words = s.split(" ")
        ct.incAll(words)

    assert ct.get(["the", "fox"]) == 2
    assert ct.get(["the", "fox", "and"]) == 1
    assert ct.get(["the", "fox", "and", "the", "moon"]) == 1
