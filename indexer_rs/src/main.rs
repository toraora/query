mod gen_file_list;
mod iterators;
mod merge;

use iterators::FixedNgramStringIterator;

use dashmap::DashMap;
use env_logger::Env;
use gxhash::GxBuildHasher;
use log::info;
use regex::Regex;
use std::fs::File;
use std::io::BufReader;
use std::io::Write;
use std::sync::Arc;
use threadpool::ThreadPool;
use unidecode::unidecode;
use xml::reader::{EventReader, XmlEvent};

const NGRAM_SIZE: usize = 1;
const NUM_THREADS: usize = 8;
const PREALLOCATE_SIZE: usize = 200_000_000;

lazy_static::lazy_static! {
    static ref XML_CLASS_REGEX: Regex = Regex::new(r#"\bclass=\\?\"[^\"]+\""#).unwrap();
    static ref XML_STYLE_REGEX: Regex = Regex::new(r#"\bstyle=\\?\"[^\"]+\""#).unwrap();
    static ref DIVIDERS_REGEX: Regex = Regex::new(r#"[-—\/]"#).unwrap();
    static ref NON_ALPHA_REGEX: Regex = Regex::new(r#"[^a-z\s\n]"#).unwrap();
    static ref SPACES_REGEX: Regex = Regex::new(r#"[^\S\n]+"#).unwrap();
}

type NgramType = String;
type NgramMapType = DashMap<NgramType, u32, GxBuildHasher>;

fn main() {
    env_logger::Builder::from_env(Env::default().default_filter_or("info")).init();

    let args = std::env::args().skip(1).collect::<Vec<String>>();
    let cmd = args.get(0).unwrap();
    if cmd == "gen_file_list" {
        gen_file_list::gen_file_list();
        return;
    } else if cmd == "merge" {
        merge::merge_data_files();
        return;
    }

    let mut count: u32 = 0;
    let pool = ThreadPool::new(NUM_THREADS);
    let ngram_frequency_map: Arc<NgramMapType> = Arc::new(DashMap::with_capacity_and_hasher(
        PREALLOCATE_SIZE,
        GxBuildHasher::default(),
    ));

    // get input files from args, passed by xargs
    let input_files = args;

    info!("Starting indexer");
    info!("Number of input files: {}", input_files.len());

    for path in input_files {
        {
            let ngram_frequency_map = Arc::clone(&ngram_frequency_map);
            pool.execute(move || process_one_file(&path, &ngram_frequency_map));
            count += 1;
        }

        // dashmap performance really poops around 200 million entries, so tweak this
        // (together with NGRAM_SIZE) to stay below that
        if count % 1830 == 0 {
            pool.join();
            info!("Processed {} files", count);
            info!(
                "Size of frequency map prior to cull: {}",
                ngram_frequency_map.len()
            );
            // drop infrequent ngrams to save memory
            ngram_frequency_map.retain(|_, v| *v > 1);
            info!(
                "Size of frequency map post cull: {}",
                ngram_frequency_map.len()
            );
        }
    }

    // dump the final ngram frequency map
    pool.join();
    dump_ngram_frequency_map(&ngram_frequency_map);

    info!("Done");

    // wait for user input before exiting
    let mut input = String::new();
    std::io::stdin().read_line(&mut input).unwrap();
}

fn dump_ngram_frequency_map(ngram_frequency_map: &NgramMapType) {
    // write ngram frequency map to file sorted in descending order, keeping
    // only ngrams that appear at least 1 times
    // use zstd to compress the file
    let mut ngram_frequency_vec: Vec<(NgramType, u32)> = ngram_frequency_map
        .iter()
        .filter(|entry| *entry.value() >= 1)
        .map(|entry| (entry.key().clone(), *entry.value()))
        .collect();
    ngram_frequency_vec.sort_by(|a, b| b.1.cmp(&a.1));
    let time = chrono::Utc::now().timestamp();
    let file_path = format!("output/ngram_{}_frequency_{}.zst", NGRAM_SIZE, time);
    let file = File::create(file_path).unwrap();
    let mut encoder = zstd::stream::Encoder::new(file, 0).unwrap();
    for (ngram, freq) in ngram_frequency_vec {
        encoder
            .write_all(format!("{}\t{}\n", ngram, freq).as_bytes())
            .unwrap();
    }
    encoder.finish().unwrap();

    ngram_frequency_map.clear();
}

fn clean_article(text: &str) -> String {
    let text = unidecode(&text);
    let text = XML_CLASS_REGEX.replace_all(&text, " ");
    let text = XML_STYLE_REGEX.replace_all(&text, " ");
    let text = text
        .replace("codice_", " ")
        .replace("colspan=", " ")
        .replace("rowspan=", " ")
        .replace(". ", "\n")
        .to_lowercase();
    let text = DIVIDERS_REGEX.replace_all(&text, " ");
    let text = NON_ALPHA_REGEX.replace_all(&text, "");
    let text = SPACES_REGEX.replace_all(&text, " ");

    if text.contains("jolly old saint ni") {
        info!("Found Santa Claus");
    }

    text.to_string()
}

fn get_ngram_strings_from_line(line: &str) -> FixedNgramStringIterator {
    FixedNgramStringIterator::new(line, NGRAM_SIZE)
}

fn process_one_file<'a>(path: &str, ngram_frequency_map: &NgramMapType) {
    let file = File::open(path).unwrap();
    let file = BufReader::new(file);
    let parser = EventReader::new(file);

    for e in parser {
        match e {
            Ok(XmlEvent::StartElement {
                name, attributes, ..
            }) => {
                if name.local_name != "doc" {
                    info!("{} {:?}", name, attributes);
                }
            }
            Ok(XmlEvent::EndElement { name }) => {
                if name.local_name != "doc" {
                    info!("{}", name);
                }
            }
            Ok(XmlEvent::Characters(s)) => {
                let cleaned = clean_article(&s);
                for line in cleaned.lines() {
                    let ngrams = get_ngram_strings_from_line(line);
                    let mut prev_ngram: Option<String> = None;
                    for ngram in ngrams {
                        if let Some(ref prev) = prev_ngram {
                            if prev == &ngram {
                                continue;
                            }
                        }
                        prev_ngram = Some(ngram.clone());

                        ngram_frequency_map
                            .entry(ngram)
                            .and_modify(|e| *e += 1)
                            .or_insert(1);
                    }
                }
            }
            Err(e) => {
                info!("Error: {}", e);
                break;
            }
            _ => {}
        }
    }
}
