use std::collections::VecDeque;

pub struct FixedNgramStringIterator<'a> {
    words: Vec<&'a str>,
    ngram_size: usize,
    current_index: usize,
}

impl<'a> FixedNgramStringIterator<'a> {
    pub fn new(line: &'a str, ngram_size: usize) -> Self {
        FixedNgramStringIterator {
            words: line.split_whitespace().collect(),
            ngram_size,
            current_index: 0,
        }
    }
}

impl<'a> Iterator for FixedNgramStringIterator<'a> {
    type Item = String;

    fn next(&mut self) -> Option<Self::Item> {
        if (self.current_index + self.ngram_size) > self.words.len() {
            return None;
        }

        let ngram =
            self.words[self.current_index..(self.current_index + self.ngram_size)].join(" ");
        self.current_index += 1;
        Some(ngram)
    }
}

pub struct NgramStringIterator<'a> {
    words: std::str::SplitWhitespace<'a>,
    buffer: VecDeque<&'a str>,
    ngram_size: usize,
    current_index: usize,
}

impl<'a> NgramStringIterator<'a> {
    pub fn new(line: &'a str, ngram_size: usize) -> Self {
        NgramStringIterator {
            words: line.split_whitespace(),
            buffer: VecDeque::new(),
            ngram_size,
            current_index: 0,
        }
    }
}

impl<'a> Iterator for NgramStringIterator<'a> {
    type Item = String;

    fn next(&mut self) -> Option<Self::Item> {
        while self.buffer.len() < self.ngram_size {
            if let Some(word) = self.words.next() {
                self.buffer.push_back(word);
            } else {
                break;
            }
        }

        if self.buffer.is_empty() {
            return None;
        }

        let mut ngram = String::with_capacity(32);
        for (i, word) in self.buffer.iter().take(self.current_index + 1).enumerate() {
            if i > 0 {
                ngram.push(' ');
            }
            ngram.push_str(word);
        }

        self.current_index += 1;
        if self.current_index == self.buffer.len() {
            self.current_index = 0;
            self.buffer.pop_front();
        }

        Some(ngram)
    }
}
