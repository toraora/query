use rand::seq::SliceRandom;
use std::fs::File;
use std::io::Write;
use walkdir::WalkDir;

pub fn gen_file_list() {
    let mut input_files = Vec::new();
    for entry in WalkDir::new("../extract/text/") {
        match entry {
            Ok(entry) => {
                if entry.file_type().is_file() {
                    let path = entry.path().to_str().unwrap().to_string();
                    input_files.push(path);
                }
            }
            Err(e) => {
                // info!("Error: {}", e);
            }
        }
    }

    // shuffle the input files
    input_files.shuffle(&mut rand::thread_rng());

    // write the file list
    let mut file = File::create("data_file_list").unwrap();
    for path in input_files {
        writeln!(file, "{}", path).unwrap();
    }
}
