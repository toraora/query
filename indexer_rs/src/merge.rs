use gxhash::HashMap;
use log::info;
use rand::prelude::SliceRandom;
use std::fs;
use std::hash::{BuildHasher, Hash, Hasher};
use std::io::{BufRead, Write};
use std::sync::mpsc::TryRecvError;
use std::sync::{Arc, Mutex};
use std::thread;

const NUM_SHARDS: usize = 512; // Power of 2 for fast modulo

struct ShardedMap<K, V> {
    shards: Vec<Mutex<HashMap<K, V>>>,
}

impl<K: Hash + Eq, V: std::ops::AddAssign> ShardedMap<K, V> {
    fn new() -> Self {
        let mut shards = Vec::with_capacity(NUM_SHARDS);
        for _ in 0..NUM_SHARDS {
            shards.push(Mutex::new(HashMap::default()));
        }
        Self { shards }
    }

    fn get_shard(&self, key: &K) -> usize {
        let mut hasher =
            std::hash::BuildHasherDefault::<gxhash::GxHasher>::default().build_hasher();
        key.hash(&mut hasher);
        (hasher.finish() as usize) & (NUM_SHARDS - 1)
    }

    fn increment_or_insert(&self, key: K, value: V) {
        let shard = self.get_shard(&key);
        let mut guard = self.shards[shard].lock().unwrap();
        if let Some(v) = guard.get_mut(&key) {
            *v += value;
        } else {
            guard.insert(key, value);
        }
    }
}

pub fn merge_data_files() {
    // grab the data files from the ./output/ directory
    let data_files = fs::read_dir("./output/")
        .unwrap()
        .map(|entry| entry.unwrap().path().to_str().unwrap().to_string())
        .collect::<Vec<String>>();

    info!("Merging {} data files", data_files.len());

    // Create a thread-safe frequency map for the final results
    let frequency_map: Arc<ShardedMap<String, u32>> = Arc::new(ShardedMap::new());

    // Create a channel for work distribution
    let (sender, receiver) = std::sync::mpsc::channel();
    let receiver = Arc::new(Mutex::new(receiver));

    // Send all work items through the channel
    for file_path in data_files {
        sender.send(file_path).unwrap();
    }
    // Drop sender to signal no more work
    drop(sender);

    // Spawn worker threads
    let num_threads = 8;
    let mut handles = Vec::with_capacity(num_threads);

    for _ in 0..num_threads {
        let receiver = Arc::clone(&receiver);
        let map = Arc::clone(&frequency_map);

        handles.push(thread::spawn(move || {
            // Thread-local map for batching updates
            let mut local_map: HashMap<String, u32> = HashMap::default();
            const BATCH_SIZE: usize = 10000;

            loop {
                // Get next file path with minimal lock duration
                let file_path = {
                    let receiver_guard = receiver.lock().unwrap();
                    match receiver_guard.try_recv() {
                        Ok(path) => path,
                        Err(TryRecvError::Empty) => continue,
                        Err(TryRecvError::Disconnected) => break,
                    }
                }; // Lock is released here

                info!("Processing {}", file_path);
                let reader =
                    zstd::stream::read::Decoder::new(fs::File::open(file_path).unwrap()).unwrap();

                let reader = std::io::BufReader::new(reader);
                for line in reader.lines() {
                    let line = line.unwrap();
                    let mut parts = line.split('\t');
                    let ngram = parts.next().unwrap().to_string();
                    let frequency = parts.next().unwrap().parse::<u32>().unwrap();

                    map.increment_or_insert(ngram, frequency);

                    // Batch update to global map
                    if local_map.len() >= BATCH_SIZE {
                        for (ngram, freq) in local_map.drain() {
                            map.increment_or_insert(ngram, freq);
                        }
                    }
                }

                // Final update for remaining entries
                for (ngram, freq) in local_map.drain() {
                    map.increment_or_insert(ngram, freq);
                }
            }
        }));
    }

    // Wait for all threads to complete
    for handle in handles {
        handle.join().unwrap();
    }

    // Convert ShardedMap to Vec for final processing
    let mut ngram_frequency_vec: Vec<(String, u32)> = frequency_map
        .shards
        .iter()
        .flat_map(|shard| {
            // Minimize lock duration by quickly collecting entries
            let entries = {
                let guard = shard.lock().unwrap();
                guard
                    .iter()
                    .map(|(k, v)| (k.clone(), *v))
                    .collect::<Vec<_>>()
            }; // Lock is released here
               // Process entries after lock is released
            entries.into_iter()
        })
        .filter(|entry| entry.1 >= 10)
        .collect();
    // shuffle the vec
    ngram_frequency_vec.shuffle(&mut rand::thread_rng());

    // ensure that the output directory exists
    fs::create_dir_all("output_final").unwrap();

    // split the vec into 32 equal parts
    let chunk_size = ngram_frequency_vec.len() / 32;
    for i in 0..32 {
        info!("Writing chunk {}", i);
        let start = i * chunk_size;
        let end = if i == 31 {
            ngram_frequency_vec.len()
        } else {
            (i + 1) * chunk_size
        };
        let chunk = &mut ngram_frequency_vec[start..end];
        // sort chunk by frequency in descending order
        chunk.sort_by(|a, b| b.1.cmp(&a.1));
        let file_path = format!("output_final/ngram_frequency_{}", i);
        let mut file = fs::File::create(file_path).unwrap();

        // write to the file in plain text
        for (ngram, freq) in chunk {
            let ngram_no_spaces = ngram.replace(" ", "");
            writeln!(file, "{}\t{}\t{}", freq, ngram, ngram_no_spaces).unwrap();
        }
    }

    // wait for stdin before exiting
    let mut input = String::new();
    std::io::stdin().read_line(&mut input).unwrap();
}
