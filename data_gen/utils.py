from collections import defaultdict


def doc_word_freq_default_factory():
    return defaultdict(int)

def get_word_id_assigner():
    word_id = 0
    def inc():
        nonlocal word_id
        word_id += 1
        return word_id
    return defaultdict(inc)