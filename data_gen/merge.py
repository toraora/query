#!/usr/bin/env python3
import sys
import pickle
import collections
import gzip
import uuid

from config import MIN_FREQ, SINGLE_MIN_FREQ
import utils

stage = sys.argv[1]
is_final = sys.argv[2] == 'final'
files = sys.argv[3:]

all_words = collections.defaultdict(int)
real_word_mapping = utils.get_word_id_assigner()

total = 0
for file_name in files:
    with gzip.open(file_name, "rb") as f:
        word_mapping, word_freq = pickle.load(f)
        # e.g. (1 -> a) and (a -> 3) turns into (1 -> 3)
        for word_id, word in list(word_mapping.items()):
            word_mapping[word_id] = real_word_mapping[word]
        for word_ids, freq in word_freq:
            word = tuple(word_mapping[word_id] for word_id in word_ids)
            all_words[word] += freq

file_name = str(uuid.uuid4()) + ".pickle.gz"
full_file_name = f"out/stage-{stage}/{file_name}"
all_words_f = gzip.open(full_file_name, "wb", compresslevel=7)

all_words_filtered = [item for item in all_words.items() if not is_final or item[1] >= MIN_FREQ]
word_dict_inverted = {word_id: word for word, word_id in real_word_mapping.items()}
pickle.dump((word_dict_inverted, all_words_filtered), all_words_f)

all_words_f.close()
