#!/usr/bin/env python3
import sys
import re
import collections
import bz2
import gzip
import pickle
import json
import logging

logging.basicConfig(
        format='%(asctime)s %(levelname)-8s %(message)s',
        level=logging.INFO,
        datefmt='%Y-%m-%d %H:%M:%S')

import unidecode
from config import NGRAMS, SINGLE_MIN_FREQ
import utils

PICKLE = True


file_names = sys.argv[1:]
logging.info(f"working on {file_names}")

word_freq = collections.defaultdict(int)
doc_word_freq = collections.defaultdict(utils.doc_word_freq_default_factory)

word_id = 0
def inc():
    global word_id
    word_id += 1
    return word_id
word_dict = collections.defaultdict(inc)


def final_clean(s):
    s = s.lower()
    s = re.sub("[-—\/]", " ", s)
    s = re.sub("[^a-z\s]", "", s)
    return s


def process_anchor(anchor):
    pass

anchor_regex = re.compile("\<a[^\<]*\>([^\<]*)\<\/a\>")
xml_class_regex = re.compile("\bclass=\\?\"[^\"]+\"")
xml_style_regex = re.compile("\bstyle=\\?\"[^\"]+\"")

def process_article(article):
    title = article['title']
    text = article['text']

    anchors = re.findall("\<a[^\<]*\<\/a\>", text)
    for anchor in anchors:
        process_anchor(anchor)
    text = re.sub(anchor_regex, "\g<1>", text)
    text = re.sub(xml_class_regex, " ", text)
    text = re.sub(xml_style_regex, " ", text)
    text = text.replace("codice_", " ")
    text = text.replace("colspan=", " ")
    text = text.replace("rowspan=", " ")
    text = unidecode.unidecode(text)
    text = text.replace("BULLET::::- ", "")
    text = final_clean(text)

    # skip the title line
    for l in text.split('\n')[1:]:
        words = l.split()
        for start in range(len(words)):
            for end in range(start + 1, min(len(words), start + NGRAMS) + 1):
                word = tuple(word_dict[word] for word in words[start:end])
                word_freq[word] += 1

for file_name in file_names:
    with bz2.open(file_name, mode="rt") as f:
        i = 0
        for l in f:
            article = json.loads(l)
            process_article(article)
            i += 1
            if i % 500 == 0:
                logging.debug(f"progress: {i}")

#sorted_word_freq = sorted(word_freq.items(), key=lambda x: -x[1])
sorted_word_freq = [item for item in word_freq.items() if item[1] >= SINGLE_MIN_FREQ]

logging.info("writing results...")

word_dict_inverted = {word_id: word for word, word_id in word_dict.items()}

if PICKLE:
    out_file = "out/" + file_name.split("/")[-1].split(".")[0] + "-freq.pickle.gz"
    out_file = gzip.open(out_file, "wb", compresslevel=7)
    pickle.dump((word_dict_inverted, sorted_word_freq), out_file)
else:
    out_file = "out/" + file_name.split("/")[-1].split(".")[0] + "-freq_test"
    out_file = gzip.open(out_file, "wt", compresslevel=7)
    for word, freq in sorted_word_freq:
        word = " ".join(word_dict_inverted[word_id] for word_id in word)
        out_file.write(f"{word}\t{freq}\n")
out_file.close()
