#!/usr/bin/env python3
import sys
import pickle
import collections
import gzip
import uuid

file_name = sys.argv[1]

with open('all_words', 'w') as f_out:
    with gzip.open(file_name, "rb") as f:
        word_mapping, word_freq = pickle.load(f)
        for word_ids, freq in word_freq:
            word = " ".join([word_mapping[word_id] for word_id in word_ids])
            word_no_spaces = word.replace(" ", "")
            f_out.write(f"{freq}\t{word}\t{word_no_spaces}\n")
        